﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstVoiceTrigger : MonoBehaviour
{

    public AudioSource audioSource;
    public Text myText;
    
    // Start is called before the first frame update
    void Start()
    {
        // myText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.gameObject.CompareTag("Player"))
        {
            audioSource.Play();
            // Enable the text so it shows 
            myText.text = "This level takes place right after Home. You're pushing this engine up a mountain. You're walking painstakingly slow. You need to get this engine to the top of the mountain.";
            Destroy(myText, 12f);
            //myText.SetActive(false); // Disable the text so it is hidden
        }
    }
}
