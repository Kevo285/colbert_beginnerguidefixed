﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ThirdText : MonoBehaviour
{
    public AudioSource audioSource;
    public Text myText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.gameObject.CompareTag("Player"))
        {
            audioSource.Play();
            // Enable the text so it shows 
            myText.text = "There, you've reached the top. Now you're congratulated by success. The success he craved is here at the top. Did you notice that engine is gone? That is because it isn't relevant anymore. This must be about how he no longer seeks success. Right?";
            Destroy(myText, 22f);
            //myText.SetActive(false); // Disable the text so it is hidden
        }
    }
}
