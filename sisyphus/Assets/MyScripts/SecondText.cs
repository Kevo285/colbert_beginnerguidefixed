﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SecondText : MonoBehaviour
{

    public AudioSource audioSource;
    public Text myText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.gameObject.CompareTag("Player"))
        {
            audioSource.Play();
            // Enable the text so it shows 
            myText.text = "You see, this level, called Sisyphus does not even make sense though. You fall through the mountain and have to climb all the way back up before you reach the top. Here. I will fix it for you. Walk to the giant engine.";
            Destroy(myText, 16f);
            //myText.SetActive(false); // Disable the text so it is hidden
        }
    }
}
